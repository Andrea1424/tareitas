import { UserServicesService } from 'src/app/services/user-services.service';

import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-agregarproyectos',
  templateUrl: './agregarproyectos.component.html',
  styleUrls: ['./agregarproyectos.component.css']
})
export class AgregarproyectosComponent implements OnInit {

  tiles: any[] = [
    {text: 'One', cols: 3, rows: 1, color: 'lightblue'},
    {text: 'Two', cols: 1, rows: 2, color: 'lightgreen'},
    {text: 'Three', cols: 1, rows: 1, color: 'lightpink'},
    {text: 'Four', cols: 2, rows: 1, color: '#DDBDF1'},
  ]; 

  form: FormGroup;

  constructor(private formbuilder: FormBuilder) { 
this.form = formbuilder.group({
nombreproyecto:['', Validators.required],
descripcion:['', Validators.required],
criterios:['', Validators.required],
apertura:['', Validators.required],
cierre:['', Validators.required],
recompensa:['', Validators.required],
email: ['', [Validators.required, Validators.pattern("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$")]],
telefono:['', Validators.required],
comentarios:['', Validators.required]


});

  }

  ngOnInit(): void {
  }
  agregarProyecto(){
    console.log(this.form)
  //   const project: Proyecttos ={ 

  //   nombreproyecto: this.form.value.nombredelproyecto,
  //   descripcion: this.form.value.descripcion,
  //   criterios: this.form.value.criterios,
  //   publicacion: this.form.value.publicacion,
  //   cierre: this.form.value.cierre,
  //   recompensa: this.form.value.recompensa,
  //   email: this.form.value.email,
  //   telefono: this.form.value.telefono,
  //   comentarios:this.form.value.comentarios
  // }
 // console.log(project);

  }

}
export class DatepickerActionsExample {}
